#!/usr/bin/env python3

import matplotlib.pyplot as plt
import argparse
import pathlib
import datetime
import os

from dotenv import load_dotenv
from sqlalchemy import select
from sqlalchemy import Column, ForeignKey, Integer, String, DateTime, UniqueConstraint, create_engine
from sqlalchemy.orm import aliased, sessionmaker

from mcusage.database import Base
from mcusage.models import User, UserEvent, UserLoggedIn

load_dotenv()
DATABASE_URL = os.getenv('DATABASE_URL', "sqlite:///./minecraft-usage.sqlite")
STATICDIR = os.getenv('STATICDIR')
if STATICDIR:
    DEFAULT_OUTDIR=pathlib.Path(STATICDIR) / 'img'
else:
    DEFAULT_OUTDIR=pathlib.Path('.')

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--outdir', type=pathlib.Path, default=DEFAULT_OUTDIR)
    parser.add_argument('--db', default=DATABASE_URL)
    args = parser.parse_args()
    return args


def transform_y_and_labels(users):
    usernames = {user.id:user.name for user in users}
    userids = [user.id for user in users]
    sorted_distinct_ids = sorted(set(userids))
    num_users = len(sorted_distinct_ids)
    starting_from_one = {}
    labels = []
    while len(starting_from_one) < num_users:
        tick = len(starting_from_one) +1
        user_id = sorted_distinct_ids.pop(0)
        starting_from_one[user_id] = tick
        labels.append(usernames[user_id])
    y = [starting_from_one[user.id] for user in users]
    return y, labels


def plot_logins(filename, users, logins, logouts, title):
    y, labels = transform_y_and_labels(users)
    t1 = [event.timestamp for event in logins]
    t2 = [event.timestamp for event in logouts]
    fig = plt.figure()
    plt.gcf().subplots_adjust(left=0.2, bottom=0.2)
    plt.hlines(y, t1, t2, color = 'red')
    plt.xticks(rotation=45, horizontalalignment='right')
    if len(y):
        ymax = max(y)
        ymin = min(y)
        plt.ylim(ymax+0.5, ymin-0.5)
        plt.yticks(range(1, ymax+1), labels)
    else:
        plt.xticks([], [])
        plt.yticks([], [])
    plt.title(title)
    fig.savefig(filename)


def get_user_events(db, user_id):
    objs = db.query(models.UserEvent, models.User, models.Event
            ).join(models.User.id == models.UserEvent.user_id
            ).join(models.Event.id == models.UserEvent.event_id
            ).filter(models.User.id == user_id
            ).all()
    return objs
    

def get_users(db):
    objs = db.query(User).all()
    return {obj.name:obj for obj in objs}


def get_user_logins(db, user):
    a1 = aliased(UserEvent)
    a2 = aliased(UserEvent)
    q = select(UserLoggedIn, a1, a2, User
            ).join(a1, UserLoggedIn.login.of_type(a1)
            ).join(a2, UserLoggedIn.logout.of_type(a2)
            ).join(a1.user
            ).where(a1.user == user
            )
    result = db.execute(q)
    return [(user, login, logout) for userloggedin, login, logout, user in result.all()]


def get_logins_after(db, date):
    a1 = aliased(UserEvent)
    a2 = aliased(UserEvent)
    q = select(UserLoggedIn, a1, a2, User
            ).join(a1, UserLoggedIn.login.of_type(a1)
            ).join(a2, UserLoggedIn.logout.of_type(a2)
            ).join(a1.user
            ).where(a1.timestamp >= date
            )
    result = db.execute(q)
    users = []
    logins = []
    logouts = []
    for userloggedin, login, logout, user in result.all():
        users.append(user)
        logins.append(login)
        logouts.append(logout)
    return users, logins, logouts


def main():
    args = parse_args()
    args.outdir.mkdir(parents=True, exist_ok=True)
    engine = create_engine(args.db)
    Base.metadata.create_all(bind=engine)
    Session = sessionmaker(engine)
    with Session() as db:
        epoch = datetime.date(1970, 1, 1)
        users, logins, logouts = get_logins_after(db, epoch)
        outfilename = args.outdir / 'minecraft-usage-alltime.png'
        title = 'alltime usage'
        plot_logins(outfilename, users, logins, logouts, title)

        today = datetime.datetime.today()
        month = datetime.timedelta(days=31)
        users, logins, logouts = get_logins_after(db, today - month)
        outfilename = args.outdir / 'minecraft-usage-monthly.png'
        title = "last month's usage"
        plot_logins(outfilename, users, logins, logouts, title)

        week = datetime.timedelta(days=7)
        users, logins, logouts = get_logins_after(db, today - week)
        outfilename = args.outdir / 'minecraft-usage-weekly.png'
        title = "last week's usage"
        plot_logins(outfilename, users, logins, logouts, title)

        day = datetime.timedelta(days=1)
        users, logins, logouts = get_logins_after(db, today - day)
        outfilename = args.outdir / 'minecraft-usage-daily.png'
        title = "last day's usage"
        plot_logins(outfilename, users, logins, logouts, title)

if __name__ == '__main__':
    main()
