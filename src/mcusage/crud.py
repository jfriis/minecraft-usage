from sqlalchemy.orm import Session

from mcusage import models, schemas

def get_user_events(db: Session):
    return db.query(models.UserEvent, models.User, models.Event).join(models.User).join(models.Event).all()
