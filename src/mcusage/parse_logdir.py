#!/usr/bin/env python3
import argparse
import datetime
import os
import pathlib

from dotenv import load_dotenv
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from mcusage.database import Base
from mcusage.parse_log import input_handle, add_from_handle, join_login_and_out

load_dotenv()

DATABASE_URL = os.getenv('DATABASE_URL', "sqlite:///./minecraft-usage.sqlite")


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('logdir', type=pathlib.Path)
    parser.add_argument('--db', default=DATABASE_URL)
    args = parser.parse_args()
    return args


def scan_dir(folder):
    paths = folder.glob('*')
    files = []
    for p in paths:
        fn = str(p.name)
        if fn.endswith('.log.gz'):
            datestr = fn[:10]
            date = datetime.date.fromisoformat(datestr)
        elif fn == 'latest.log':
            t = os.path.getmtime(p)
            date = datetime.date.fromtimestamp(t)
        else:
            raise Exception(f"Unknown file: {p}")
        files.append((p, date))
    return files


def main():
    args = parse_args()
    engine = create_engine(args.db)
    Base.metadata.create_all(bind=engine)
    Session = sessionmaker(engine)
    files = scan_dir(args.logdir)
    with Session() as db:
        for path, date in files:
            handle = input_handle(path)
            add_from_handle(db, handle, date)
            handle.close()
        join_login_and_out(db)


if __name__ == '__main__':
    main()
