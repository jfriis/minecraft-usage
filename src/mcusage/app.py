from typing import List
import importlib.resources
import os.path
from distutils.dir_util import copy_tree

from dotenv import load_dotenv
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from fastapi import Depends, FastAPI, HTTPException, Request
from sqlalchemy.orm import Session

from mcusage import crud, models, schemas
from mcusage.database import Base, SessionLocal, engine

load_dotenv()

Base.metadata.create_all(bind=engine)

app = FastAPI()

staticdir = os.path.dirname(os.path.abspath(__file__)) + '/static'
#with importlib.resources.path('mcusage', 'static') as staticdir:
STATICDIR = os.getenv('STATICDIR', staticdir)
if not os.path.isdir(STATICDIR):
    copy_tree(staticdir, STATICDIR)
app.mount("/static", StaticFiles(directory=STATICDIR), name="static")

templatesdir = os.path.dirname(os.path.abspath(__file__)) + '/templates'
#with importlib.resources.path('mcusage', 'templates') as templatesdir:
TEMPLATESDIR = os.getenv('TEMPLATESDIR', templatesdir)
if not os.path.isdir(TEMPLATESDIR):
    copy_tree(templatesdir, TEMPLATESDIR)
templates = Jinja2Templates(directory=TEMPLATESDIR)

# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

@app.get("/", response_class=HTMLResponse)
async def root(request: Request):
    return templates.TemplateResponse("usage.html", {"request": request})


@app.get("/usage/", response_model=List[schemas.JoinUserEvent])
async def read_user_events(db: Session = Depends(get_db)):
    user_events = crud.get_user_events(db)
    return user_events
