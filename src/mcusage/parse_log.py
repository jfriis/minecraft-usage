#!/usr/bin/env python3
import importlib.resources
import argparse
import datetime
import gzip
import os
import re
import sys

from dotenv import load_dotenv
from sqlalchemy import Column, ForeignKey, Integer, String, DateTime, UniqueConstraint, create_engine
from sqlalchemy import select
from sqlalchemy.orm import sessionmaker

from mcusage.database import Base
from mcusage.models import User, Event, UserEvent, UserLoggedIn

load_dotenv()

DATABASE_URL = os.getenv('DATABASE_URL', "sqlite:///./minecraft-usage.sqlite")

EVENTS = {}
EVENTS['login'] = re.compile(r' (?P<nick>\S+) joined the game')
EVENTS['logout'] = re.compile(r'\[Server thread/INFO\]: (?P<nick>\S+) lost connection')

REGEX_DISALLOWED_NICKS = re.compile(r'(?:[0-9]\.|1\d?\d?\.|2[0-5]?[0-5]?\.){3}(?:[0-9]|1\d?\d?|2[0-5]?[0-5]?)')


def extract_time(line):
    # [00:00:01] [Server thread/INFO]: Automatic saving is now disabled
    ints = list(map(int, line[1:9].split(':')))
    return datetime.time(*ints)


def add_time(d, t):
    return datetime.datetime(year=d.year, month=d.month, day=d.day, hour=t.hour, minute=t.minute, second=t.second)


def input_handle(arg):
    try:
        handle = gzip.open(arg, 'rt')
        handle.read(2)
        handle.seek(0)
        return handle
    except gzip.BadGzipFile:
        return open(arg)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('file', nargs='?', type=input_handle)
    parser.add_argument('--date', required=True)
    parser.add_argument('--db', default=DATABASE_URL)
    args = parser.parse_args()
    if not args.file:
        if sys.stdin.isatty():
            sys.exit("Please provide an input file, or pipe it via stdin")
        else:
            args.file = sys.stdin
    try:
        args.date = datetime.date(*map(int, args.date.split('-')))
    except:
        raise
        sys.exit("Please provide --date in ISO format 'YYYY-MM-DD'")
    return args


def add_user(db, name):
    obj = User(name=name)
    db.add(obj)
    db.commit()
    db.refresh(obj)
    return obj
    

def add_event(db, name):
    obj = Event(name=name)
    db.add(obj)
    db.commit()
    db.refresh(obj)
    return obj


def add_user_event(db, timestamp, user: User, event: Event):
    obj = UserEvent(timestamp=timestamp, user_id=user.id, event_id=event.id)
    db.add(obj)
    db.commit()
    db.refresh(obj)
    return obj


def get_users(db):
    objs = db.query(User).all()
    return {obj.name:obj for obj in objs}

    
def get_events(db):
    objs = db.query(Event).all()
    return {obj.name:obj for obj in objs}
    

def get_user_events(db):
    objs = db.query(UserEvent).all()
    return objs
    

def get_user_event(db, timestamp, user, event):
    return db.query(UserEvent).filter(
            UserEvent.timestamp==timestamp,
        UserEvent.user_id==user.id,
        UserEvent.event_id==event.id
        ).all()


def join_login_and_out(db):
    login_event_id = db.query(Event.id).filter(Event.name == 'login').scalar()
    logout_event_id = db.query(Event.id).filter(Event.name == 'logout').scalar()
    users = get_users(db)
    registered_logins = set(db.execute(select(UserLoggedIn.login_id)).scalars())
    registered_logouts = set(db.execute(select(UserLoggedIn.logout_id)).scalars())
    registered_user_events = registered_logins.union(registered_logouts)
    for user in users.values():
        q = select(UserEvent
                ).join(Event
                ).where(UserEvent.user_id == user.id
                ).where(UserEvent.id.notin_(registered_user_events)
                ).order_by(UserEvent.timestamp
                )
        result = db.execute(q)
        login_id = None
        user_events = list(result.scalars())
        ue_timestamps = [(ue.timestamp, ue.event.name) for ue in user_events]
        for ue in user_events:
            if ue.event_id == login_event_id:
                login_id = ue.id
            elif ue.event_id == logout_event_id:
                if login_id is not None:
                    obj = UserLoggedIn(login_id=login_id, logout_id=ue.id)
                    db.add(obj)
                    db.commit()


def add_from_handle(db, handle, date):
    users = get_users(db)
    events = get_events(db)
    user_events = get_user_events(db)
    for line in handle:
        if not line.startswith('['):
            continue
        for eventname, pattern in EVENTS.items():
            m = pattern.search(line)
            if m:
                break
        else:
            continue
        nick = m.group('nick')
        if REGEX_DISALLOWED_NICKS.search(nick):
            continue
        dt = add_time(date, extract_time(line))
        if not nick in users:
            user = add_user(db, nick)
            users[nick] = user
        else:
            user = users[nick]
        if not eventname in events:
            event = add_event(db, eventname)
            events[eventname] = event
        else:
            event = events[eventname]
        user_event = get_user_event(db, dt, user, event)
        if not user_event:
            user_event = add_user_event(db, dt, user, event)


def main():
    args = parse_args()
    engine = create_engine(args.db)
    Base.metadata.create_all(bind=engine)
    Session = sessionmaker(engine)
    with Session() as db:
        add_from_handle(db, args.file, args.date)
        join_login_and_out(db)


if __name__ == '__main__':
    main()
