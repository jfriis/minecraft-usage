from sqlalchemy import Column, ForeignKey, Integer, String, DateTime, UniqueConstraint
from sqlalchemy.orm import relationship

from mcusage.database import Base


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, unique=True, index=True)


class Event(Base):
    __tablename__ = "events"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, unique=True, index=True)


class UserEvent(Base):
    __tablename__ = "user_events"
    __table_args__ = (UniqueConstraint('timestamp', 'user_id', 'event_id'),)

    id = Column(Integer, primary_key=True, index=True)
    timestamp = Column(DateTime, index=True)
    user_id = Column(Integer, ForeignKey("users.id"), index=True)
    event_id = Column(Integer, ForeignKey("events.id"), index=True)

    user = relationship("User", foreign_keys=[user_id])
    event = relationship("Event", foreign_keys=[event_id])


class UserLoggedIn(Base):
    __tablename__ = "user_logged_in"
    __table_args__ = (UniqueConstraint('login_id', 'logout_id'),)

    id = Column(Integer, primary_key=True, index=True)
    login_id = Column(Integer, ForeignKey("user_events.id"), index=True)
    logout_id = Column(Integer, ForeignKey("user_events.id"), index=True)

    login = relationship("UserEvent", foreign_keys=[login_id])
    logout = relationship("UserEvent", foreign_keys=[logout_id])
