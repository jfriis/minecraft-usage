from datetime import datetime
from typing import List, Optional

from pydantic import BaseModel


class UserBase(BaseModel):
    name: str


class UserCreate(UserBase):
    pass


class User(UserBase):
    id: int

    class Config:
        orm_mode = True 


class EventBase(BaseModel):
    name: str


class EventCreate(EventBase):
    pass


class Event(EventBase):
    id: int

    class Config:
        orm_mode = True


class UserEventBase(BaseModel):
    timestamp: datetime
    user_id: int
    event_id: int


class UserEvent(UserEventBase):
    id: int

    class Config:
        orm_mode = True


class JoinUserEvent(BaseModel):
    UserEvent: UserEvent
    User: User
    Event: Event

    class Config:
        orm_mode = True
