===============
minecraft-usage
===============

Extract login and logout events from minecraft server logs to SQLite.
Plot player usage in daily, weekly, monthly, and alltime chunks.
Serve plots using FastAPI.

Usage
=====

.. code:: shell

   pip install git+https://gitlab.com/jfriis/minecraft-usage.git
   export DATABASE_URL=sqlite:///./minecraft-usage.sqlite 
   export STATICDIR=./static
   mcusage-parse-log --date $(date "+%Y-%m-%d") /opt/minecraft/worlds/ec2mc/logs/latest.log
   mcusage-plot
   pip install uvicorn
   uvicorn mcusage:app 


Deploy
======

Using Ansible.

Create `inventory`

.. code:: shell

   [minecraft]
   minecraft.example.com    ansible_host=minecraft.example.com ansible_user=root ansible_python_interpreter=/usr/bin/python3

Deploy

.. code:: shell

   ansible-playbook -i inventory www.yml -e basic_auth_password=1234 -e basic_auth_user=minecraft -e fqdn=minecraft.example.com

Works well with minecraft server deployed with emsm, see https://gitlab.com/jfriis/ec2_minecraft
