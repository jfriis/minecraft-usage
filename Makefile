SHELL = /bin/bash


all: install


.PHONY:

install:
	pip install -r requirements.txt


runserver:
	uvicorn sql_app.main:app --reload
